---
date: 2019-07-26
title: HMEE325 - Software development for robotics
categories:
  - Software development
description: "Git, CMake, ROS, C++"
type: Module
set: hmee325
set_order: 0
highlighter: rouge
---

In this module, you will learn some fondamentals about software development and especially what is needed in robotics.

This will cover:
 * [Git](/git/hmee325-git/) for source code versioning and collaboration with others
 * [CMake](/cmake/hmee325-cmake/) to easily build your C++ projects
 * [ROS](/ros/hmee325-ros/) to build complex robotics applications using standard tools

We will conclude this module with a small [project](/software%20development/hmee325-project/) to apply all these new concepts.

You can follow the links in the series below to access this module's materials